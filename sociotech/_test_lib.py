#!/usr/bin/env python

import numpy as np
import pymc3 as pm
import matplotlib.pyplot as plt

import blocks
import sociotech


def test_blocks():
    data = np.cumsum( np.random.randn(100, 2), axis=0 )

    with pm.Model() as test_model:
        vol = blocks.ExpVol( 
                data.shape
                )
        sllt = blocks.SLLT( 
                data.shape,
                vol=vol()
                )

        state_space = blocks.LinearStateSpace( 
                data.shape,
                sllt,
                observed=data
                )


def test_sociotech():

    data = np.cumsum( np.random.randn(500, 1), axis=0 )

    with pm.Model() as test_model:

        
        vol = blocks.ExpVol( 
                data.shape,
                )
        sllt = blocks.SLLT( 
                data.shape,
                vol=vol
                )
        state_space = blocks.LinearStateSpace( 
                data.shape,
                sllt,
                observed=data
                )
        
        model = sociotech.TimeSeriesModel()
        model.add_blocks( 
                vol, sllt, state_space
                )
        model.build()

        print(model.topo_sort)

        summary = model.summarize(verbose=True)

        exit()

        trace = pm.sample(2000, chains=2)
        ppc = pm.sample_posterior_predictive(trace, 500)

    
    fig, ax = plt.subplots(figsize=(10, 5))
    for zz in ppc['LSS1z_observed'][::50]:
        ax.plot(zz, 'k-', linewidth=0.5)

    plt.show()


def _test_sample():

    with pm.Model() as test_model:

        shape = (100,)

        vol = 0.1 
        llt = blocks.LLT( 
                shape, 
                vol=vol
                )
        
        model = sociotech.TimeSeriesModel()
        model.add_blocks( 
             llt
            )
        model.build()

        test = llt.sample( n_draws=10 )
        
        for x in test:
            plt.plot(x, 'k-')
        plt.show()






if __name__ == "__main__": 
    _test_sample()
