import collections

import numpy as np
import pymc3 as pm


class Block:
    """Base class for time series blocks.

    We define a block as a high-level component of a structured time series model. 
    
    """
    def __init__(self, shape, *args, **kwargs):
        self.shape = shape
        self.observed = kwargs.get('observed', None)

        # for building dependency graph in sociotech.*Model
        self.parents = dict()
        self.children = dict()
        self.order = []

    def _check_number(self, var):
        """Tries to check if a parameter is a number or not.
        """
        if ('pymc3' not in str(type(var))) and ('blocks' not in str(type(var))) and (var is not None):
            return True
        return False

    def _to_block(self, value):
        if self._check_number( value ):
            # it's a number or array-like
            if isinstance(value, collections.abc.Iterable):
                shape = value.shape  # needs to have a .shape method defined
            else:
                shape = (1,)
            value = Deterministic(shape, value)
        elif 'pymc3' in str(type(value)):
            value = _Pymc3Wrapper( value.shape, value )
        # if it's neither, it's because it was either None or already a block
        # if it's a block, great
        # if it's None, we have to deal with that later in _setup* methods.
        return value

    def _get_possible_orders(self):
        if len(self.parents.keys()) == 0:
            self.order.append( 0 )
            return
        for pa in self.parents.keys():
            self.order.append( np.max(self.parents[pa].order) + 1 )
            
    def _setup_params(self, *args, **kwargs):
        pass

    def _check_1d(self):
        if len(self.shape) == 1:
            return True
        return False


class MetaBlock:
    def __init__(self, shape, block, *args, **kwargs):
        self.shape = shape
        self.block = block

        # for building dependency graph in sociotech.*Model
        self.parents = dict()
        self.children = dict()
        self.order = []


class _Pymc3Wrapper(Block):
    """Wraps a pymc3 object in a Block structure.

    This class maintains all functionality of the underlying pymc3 object by just calling __call__.
    """
    
    def __init__(self, shape, pymc3_object, *args, **kwargs):
        super(_Pymc3Wrapper, self).__init__(self, shape, *args, **kwargs)
        self.id_ = pymc3_object.name 
        self.shape = shape
        self.pymc3_object = pymc3_object

        self.order = [ 0 ]  # guaranteed to be a root node

    def sample(self, *args, **kwargs):
        """Calls self.pymc3_object .random method
        """
        n_draws = kwargs.get('n_draws', 1)
        return self.pymc3_object.random(size=n_draws)

    def __call__(self):
        return self.pymc3_object


class LinearStateSpace(MetaBlock, Block):

    count = 0
    
    @classmethod
    def next_id(cls):
        cls.count += 1
        return f'LSS{cls.count}'

    def __init__(self,
            shape,
            block,
            *args,
            B=None,
            state_vol=None,
            observed=None,
            **kwargs
            ):
        super(LinearStateSpace, self).__init__(self, shape, *args, **kwargs)
        self.id_ = type(self).next_id()
        self.shape = shape
        self.block = block
        self.one_d = self._check_1d()

        # make sure everything is block-like
        self.B = self._to_block( B )
        self.state_vol = self._to_block( state_vol )
        # set up parameters
        self.B, self.state_vol = self._setup_params(
            self.B,
            self.state_vol
                )
        # add attributes
        self.parents[self.B.id_] = self.B
        self.B.children[self.id_] = self

        self.parents[self.state_vol.id_] = self.state_vol
        self.state_vol.children[self.id_] = self

        self.parents[self.block.id_] = self.block
        self.block.children[self.id_] = self

        self.observed = self._check_observed(
                observed
                )

        self._get_possible_orders()

        self._setup_z()

    def _check_observed(self, observed):
        if (observed is not None) and (observed.shape != self.shape):
            raise ValueError('''
            shape of observed data is {observed.shape} but my shape is {self.shape}
            ''')
        observed = pm.Data(self.id_ + 'observed', observed)
        return observed

    def _setup_params(self, B, state_vol):
        # if 1d, shape of variable is 1. Otherwise it's whatever the rest of dims are
        if self.one_d:
            var_shape = 1
        else:
            var_shape = self.shape[1:]
            if len(var_shape) > 1:
                raise ValueError('Currently LinearStateSpace supports only scalar and vector time series')
        # if a parameter is none, set it to be something sensible
        if B is None:
            shape = (var_shape[0], var_shape[0])
            B = pm.Normal(self.id_ + 'B', 0, 0.1, shape=shape)
            B = _Pymc3Wrapper( shape, B ) 
        if state_vol is None:
            state_vol = pm.InverseGamma(self.id_ + 'state_vol', alpha=3.0, beta=0.5, shape=var_shape)
            state_vol = _Pymc3Wrapper( var_shape, state_vol )
        return B, state_vol

    def _setup_z(self):
        # linear state space: z = B @ x + state_vol
        if self.one_d:
            mu_z = self.B() * self.block()
        else:
            mu_z = pm.math.dot( self.block(), self.B() )
        if self.observed is None:
            self.z = pm.Normal(self.id_ + 'z', 
                    mu=mu_z, 
                    sd=self.state_vol(),
                    shape=self.shape)
            self.z = _Pymc3Wrapper( self.shape, self.z )
        else:
            self.z = pm.Normal(self.id_ + 'z_observed', 
                    mu=mu_z, 
                    sd=self.state_vol(),
                    shape=self.shape,
                    observed=self.observed)
            self.z = _Pymc3Wrapper( self.shape, self.z, observed=self.observed )

    def __call__(self):
        return self.z()


class Deterministic(Block):

    count = 0
    
    @classmethod
    def next_id(cls):
        cls.count += 1
        return f'DB{cls.count}'

    def __init__(self,
            shape,
            value,
            *args,
            **kwargs
            ):
        super(Deterministic, self).__init__(self, shape, *args, **kwargs)
        self.id_ = type(self).next_id()
        self.shape = shape
        self.value = value
        self.order = [ 0 ]
        self.one_d = self._check_1d()

    def sample(self, *args, **kwargs):
        n_draws = kwargs.get('n_draws', 1)
        return np.array( [self.value for _ in range(n_draws)] )

    def __call__(self):
        return self.value


class ExpVol(Block):

    count = 0
    
    @classmethod
    def next_id(cls):
        cls.count += 1
        return f'EXPVOL{cls.count}'

    def __init__(self,
            shape,
            *args,
            volvol=None,
            **kwargs
            ):
        super(ExpVol, self).__init__(self, shape, *args, **kwargs)
        self.id_ = type(self).next_id()
        self.shape = shape
        self.one_d = self._check_1d()

        # make sure everything is Block-like 
        self.volvol = self._to_block( volvol )
        self.volvol = self._setup_params(
            self.volvol
                )
        # add attributes to parents and children
        self.parents[self.volvol.id_] = self.volvol
        self.volvol.children[self.id_] = self

        self._get_possible_orders()
        
        # now actually set up my priors
        self._setup_logvol()

    def _setup_params(self, volvol):
        # if 1d, shape of variable is 1. Otherwise it's whatever the rest of dims are
        if self.one_d:
            var_shape = 1
        else:
            var_shape = self.shape[1:]
        # if a parameter is none, set it to be something sensible
        if volvol is None:
            volvol = pm.InverseGamma(self.id_ + 'volvol', alpha=3.0, beta=0.5, shape=var_shape)
            volvol = _Pymc3Wrapper( var_shape, volvol )
            return volvol
        # it was already some kind of block
        return volvol

    def _setup_logvol(self):
        shape = (self.shape[0] - 1,) if self.one_d else (self.shape[0] - 1, *self.shape[1:])
        self.logvol = pm.distributions.timeseries.GaussianRandomWalk(
            self.id_ + 'logvol',
            mu=0,
            sd=self.volvol(),
            shape=shape
            )
        self.logvol = _Pymc3Wrapper( shape, self.logvol )

    def __call__(self):
        return pm.math.exp( self.logvol() )


class LLT(Block):

    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f'LLT{cls.count}'

    def __init__(self,
            shape,
            *args,
            s_mu=None,
            s_vol=None,
            vol=None,
            **kwargs
            ): 
        super(LLT, self).__init__(shape, *args, **kwargs)
        self.id_ = type(self).next_id()
        self.shape = shape
        self.one_d = self._check_1d()

        # attempt casting to blocks
        self.s_mu = self._to_block( s_mu )
        self.s_vol = self._to_block( s_vol )
        self.vol = self._to_block( vol )

        # do we need to define these parameters ourselves?
        self.s_mu, self.s_vol, self.vol = self._setup_params(
                self.s_mu,
                self.s_vol,
                self.vol
                )

        # add all parents and children to the right place
        self.parents[self.s_mu.id_] = self.s_mu
        self.s_mu.children[self.id_] = self

        self.parents[self.s_vol.id_] = self.s_vol
        self.s_vol.children[self.id_] = self

        self.parents[self.vol.id_] = self.vol
        self.vol.children[self.id_] = self

        self._get_possible_orders()

        # latent variables
        self.s = None
        self.mu = None
        self._setup_s()
        self._setup_mu()

    def sample(self, *args, **kwargs):
        """Draws random samples from the local linear trend (LLT) model.

        """
        n_draws = kwargs.get('n_draws', 1)
        # get random draws from the hidden rv instantiated here
        # this is the latent s random walk
        if len(self.s.shape) == 1:
            draw_shape = (n_draws, self.s.shape[0] + 1, 1)
            s_noise_draws = np.random.randn( *draw_shape ).reshape( *draw_shape[:-1] )
        else:
            draw_shape = (n_draws, self.s.shape[0] + 1, *self.s.shape[1:])
            s_noise_draws = np.random.randn( *draw_shape )
    
        s_vol_draws = self.s_vol.sample( n_draws=n_draws ).reshape(-1, *draw_shape[2:])
        s_mu_draws = self.s_mu.sample( n_draws=n_draws ).reshape(-1, *draw_shape[2:])
        s_draws = np.cumsum( s_mu_draws + s_vol_draws * s_noise_draws, axis=1 )

        # now draw from the actual random walk process with mean s
        vol_draws = self.vol.sample( n_draws=n_draws ).reshape(-1, *draw_shape[2:])
        mu_noise_draws = np.random.randn( n_draws, self.mu.shape[-1] )
        mu_draws = np.cumsum( s_draws + vol_draws * mu_noise_draws, axis=1 )

        return mu_draws

    def _setup_s(self):
        """Set up latent random walk slope in local trend model.

        We do not register this block's pa and ch since its simulation will be handled internally.
        """
        shape = (self.shape[0] - 1,) if self.one_d else (self.shape[0] - 1, *self.shape[1:])
        self.s = pm.distributions.timeseries.GaussianRandomWalk(
            self.id_ + 'grw',
            mu=self.s_mu(),
            sd=self.s_vol(),
            shape=shape
            )
        self.s = _Pymc3Wrapper( shape, self.s ) 

    def _setup_mu(self):
        """Set up latent random walk mean in local trend model.

        We do not register this block's pa and ch since its simulation will be handled internally.
        """
        self.mu = pm.distributions.timeseries.GaussianRandomWalk(
            self.id_ + 'level',
            mu=self.s(),
            sd=self.vol(),
            shape=self.shape
                )
        self.mu = _Pymc3Wrapper( self.shape, self.mu )

    def _setup_params(self, s_mu, s_vol, vol):
        # if 1d, shape of variable is 1. Otherwise it's whatever the rest of dims are
        if self.one_d:
            var_shape = 1
        else:
            var_shape = self.shape[1:]
        # if a parameter is none, set it to be something sensible
        if s_mu is None:
            s_mu = pm.Normal(self.id_ + 's_mu', 0, 0.1, shape=var_shape)
            s_mu = _Pymc3Wrapper( var_shape, s_mu )
        if s_vol is None:
            s_vol = pm.InverseGamma(self.id_ + 's_vol', alpha=3.0, beta=0.5, shape=var_shape)
            s_vol = _Pymc3Wrapper( var_shape, s_vol )
        if vol is None:
            vol = pm.InverseGamma(self.id_ + 'vol', alpha=3.0, beta=0.5, shape=var_shape)
            vol = _Pymc3Wrapper( var_shape, vol )
        return s_mu, s_vol, vol

    def __call__(self, *args, **kwargs):
        return self.mu()


class SLLT(Block):
    count = 0

    @classmethod
    def next_id(cls):
        cls.count += 1
        return f'SLLT{cls.count}'

    def __init__(self,
            shape,
            *args,
            s_k=None,
            s_vol=None,
            vol=None,
            **kwargs
            ): 
        super(SLLT, self).__init__(shape, *args, **kwargs)
        self.id_ = type(self).next_id()
        self.shape = shape
        self.one_d = self._check_1d()

        # attempt casting to blocks
        self.s_k = self._to_block( s_k )
        self.s_vol = self._to_block( s_vol )
        self.vol = self._to_block( vol )
    
        # do we need to define these parameters ourselves?
        self.s_k, self.s_vol, self.vol = self._setup_params(
                self.s_k,
                self.s_vol,
                self.vol
                )

        # add all parents and children to the right place
        self.parents[self.s_k.id_] = self.s_k
        self.s_k.children[self.id_] = self

        self.parents[self.s_vol.id_] = self.s_vol
        self.s_vol.children[self.id_] = self

        self.parents[self.vol.id_] = self.vol
        self.vol.children[self.id_] = self

        self._get_possible_orders()

        # latent variables
        self.s = None
        self.mu = None
        self._setup_s()
        self._setup_mu()

    def _setup_s(self):
        shape = (self.shape[0] - 1,) if self.one_d else (self.shape[0] - 1, *self.shape[1:])
        self.s = pm.distributions.timeseries.AR1(
            self.id_ + 'ar1',
            k=self.s_k(),
            tau_e=1./self.s_vol()**2.,
            shape=shape
            )
        self.s = _Pymc3Wrapper( shape, self.s )

    def _setup_mu(self):
        self.mu = pm.distributions.timeseries.GaussianRandomWalk(
            self.id_ + 'level',
            mu=self.s(),
            sd=self.vol(),
            shape=self.shape
                )
        self.mu = _Pymc3Wrapper( self.shape, self.mu )

    def _setup_params(self, s_k, s_vol, vol):
        # if 1d, shape of variable is 1. Otherwise it's whatever the rest of dims are
        if self.one_d:
            var_shape = 1
        else:
            var_shape = self.shape[1:]
        # if a parameter is none, set it to be something sensible
        if s_k is None:
            s_k = pm.Normal(self.id_ + 's_k', 0.5, 0.1, shape=var_shape)
            s_k = _Pymc3Wrapper( var_shape, s_k )
        if s_vol is None:
            s_vol = pm.InverseGamma(self.id_ + 's_vol', alpha=3.0, beta=0.5, shape=var_shape)
            s_vol = _Pymc3Wrapper( var_shape, s_vol )
        if vol is None:
            vol = pm.InverseGamma(self.id_ + 'vol', alpha=3.0, beta=0.5, shape=var_shape)
            vol = _Pymc3Wrapper( var_shape, vol )
        return s_k, s_vol, vol

    def __call__(self, *args, **kwargs):
        return self.mu()
