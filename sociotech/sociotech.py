import numpy as np
import pymc3 as pm
import networkx as nx
import matplotlib.pyplot as plt

import blocks


class TimeSeriesModel:

    def __init__(self, *args, **kwargs):
        self.nodes = dict()

        self.topo_sort = []
        self.dag = nx.DiGraph()

    def add_blocks(self, *blocks, overwrite=False):
        """Add blocks to the model.

        :*args blocks: un-named positional arguments each of which is a block.
        """
        if overwrite is False:
            for block in blocks:
                if block.id_ not in self.nodes.keys():
                    self.nodes[block.id_] = block
                    for pa in block.parents.keys():
                        if pa not in self.nodes.keys():
                            self.nodes[pa] = block.parents[pa]
                else:
                    print(f'{block.id_} already has a defined block. If you want to overwrite, pass overwrite=True.')
                    continue
                
        else:
            for block in blocks:
                self.nodes[block.id_] = block
                for pa in block.parents.keys():
                    self.nodes[pa] = block.parents[pa]

    def build(self):
        self._build_graph()
        self._build_topo_sort()

    def _build_graph(self):
        """Builds the random variable dependency graph.

        """
        for name in self.nodes.keys():
            self.dag.add_node( name )
            # parents
            self.dag.add_edges_from( [(x, name) for x in self.nodes[name].parents.keys()] )

    def _build_topo_sort(self):
        """Builds a topological sort of the random variable dependency graph.
        """
        # collect all rvs and their orders
        topo = [( name, np.max(self.nodes[name].order) ) for name in self.nodes.keys()]
        self.topo_sort = sorted(topo, key=lambda t: t[-1])

    def summarize(self, verbose=True):
        pass
